package com.example.hp10.customlist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import org.w3c.dom.Text

class CustomAdapter(var context:Context, var sweet:ArrayList<Sweet>):BaseAdapter() {

    private class ViewHolder(row:View?){
        var txtName: TextView
        var ivImage: ImageView

        init{
            this.txtName = row?.findViewById(R.id.txtName) as TextView
            this.ivImage=row.findViewById(R.id.ivSweet) as ImageView
        }

    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view:View?
        var viewHolder: ViewHolder
        if(convertView==null){//3ashan eza el row empty ye3mal inflator w y3abeeh
            var layout = LayoutInflater.from(context)
            view = layout.inflate(R.layout.sweet_listview, parent, false)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder

        }
        else{
            view = convertView
            viewHolder = view.tag as ViewHolder

        }
        var sweet :Sweet=getItem(position) as Sweet
        viewHolder.txtName.text = sweet.name
        viewHolder.ivImage.setImageResource(sweet.Img)
        return view as View
    }

    override fun getItem(position: Int): Any {
       return sweet.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return sweet.count()
    }
}