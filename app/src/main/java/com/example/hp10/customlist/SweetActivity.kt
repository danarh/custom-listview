package com.example.hp10.customlist

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ListView

class SweetActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var ListView = findViewById(R.id.listView) as ListView
        var arrSweet :ArrayList<Sweet> = ArrayList()

        arrSweet.add(Sweet("Donut" , R.drawable.donut))
        arrSweet.add(Sweet("Cookie" , R.drawable.cookie))
        arrSweet.add(Sweet("Lolipop" , R.drawable.loli))
        arrSweet.add(Sweet("Ice-cream" , R.drawable.ice))
        arrSweet.add(Sweet("Chocolate" , R.drawable.choco))
        arrSweet.add(Sweet("Cake" , R.drawable.cake))
        arrSweet.add(Sweet("Cupcake" , R.drawable.cup))

        ListView.adapter = CustomAdapter(applicationContext, arrSweet)






    }
}